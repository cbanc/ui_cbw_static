var h = require('mercury').h;
var anchor = require('ui_link');
var ui_assets = require('ui_assets');

module.exports = function (state) {
  state = state || {};
  var view_model = state.view_model || {};
  var page_model = state.page_model || {};
  var assets = ui_assets(state);

  return h('section#hero', [
    h('header', [
      assets('vm/vm_dashboard.png', {
        alt: 'Community-powered Vendor Management'
      }),
      h('h1', 'Community-powered Vendor Management'),
      h('h2', 'Stay connected. Informed. Compliant.'),
      h('h3', 'Now available from CBANC'),
      anchor({
        text: 'Learn More',
        href: view_model.sign_up_url
      })
    ])
  ]);
};
