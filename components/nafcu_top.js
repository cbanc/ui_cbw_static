var h = require('mercury').h;
var anchor = require('ui_link');
var ui_assets = require('ui_assets');

module.exports = function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var assets = ui_assets(state);

	return h('section#hero', [
		h('header', [
      h("span",
      	assets('nafcu/nafcu-logo.png', {alt: 'NAFCU'})
    	),
			h('h1', 'NAFCU Partners with CBANC to Further Help Credit Unions Stay Compliant'),
			h('p', [
				anchor({
					text: 'Join 1,400+ credit unions today!',
					href: '#'
				}, 'learn_more')
			])
		])
	]);
};
