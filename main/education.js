var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var signature = require('ui_signature');

var webinar_vid = function (view_model, video_link) {

  if (view_model.show_webinars) {
    return h('iframe.sproutvideo-player', {
      'src': video_link,
      'frameborder': '0',
      'allowfullscreen': 'true'
    });
  }

  return h('section.log_in', 'Sign in or Join the CBANC Network to watch this Webinar')
}

var testimonials = function (testimonials) {
  var testimonials = testimonials.created_by_user || [];

  if (testimonials && testimonials.length) {

    return h("ul#sig_menu",
      testimonials.map(function (t, i) {
        var item;

        item = h("div.active", [
          h("span", JSON.stringify(t))
        ]);

        return h("li", item);
      })
    );
  }

  return null;
}

var sig_check = function (testimonial) {
  if (testimonial) {
    var org = testimonial.org;
    var created_date = testimonial.created_date;

    return signature(testimonial, org, created_date, 'Quoted');
  }
  return h('p.cbanc_member', 'CBANC Member');
}


module.exports = versionify(function (state) {
  state = state || {};
  var view_model = state.view_model || {};
  var page_model = state.page_model || {};
  var testimonials = view_model.testimonials || {};

  return h('article', [

    h('h3.section_title',
      h('span', 'Experts providing live online training with uncapped playbacks. Annual membership available for unlimited institution-wide education access.')
    ),

    h('hr'),

    h('h4.sample_webinar_headline', 'Sample Webinars for CBANC Members'),
    h('hr'),

    h('section#sample_webinars', [

      h('div#webinar1.video', [
        h('h3', 'Proactive Risk Management for Community Banks'),
        webinar_vid(view_model, 'https://videos.sproutvideo.com/embed/a49bdbb71215e5c42c/80f00b18820d9219?type=sd'),
        h('p.date', 'April 23, 2015'),
        h('p.speaker', 'Speaker: Peter Cherpack - Senior Vice President, Principal & Director of Credit Technology - Ardmore Banking Advisors, Inc.'),
        anchor({
            href: (view_model.show_webinars ? 'https://videos.sproutvideo.com/embed/a49bdbb71215e5c42c/80f00b18820d9219?type=sd' : '/login'),
            text: 'Watch Now',
            target: '_blank'
          },
          'learn_more')
      ]),

      h('div#webinar2.video', [
        h('h3', '2015 Cybersecurity Update for Risk and Compliance Officers'),
        webinar_vid(view_model, 'https://videos.sproutvideo.com/embed/1c9bdab11416e7ca94/f3d1213458e5b9be?type=sd'),
        h('p.date', 'March 26, 2015'),
        h('p.speaker', 'Speaker: Joram Borenstein - Vice President - NICE Actimize'),
        anchor({
          href: (view_model.show_webinars ? 'https://videos.sproutvideo.com/embed/1c9bdab11416e7ca94/f3d1213458e5b9be?type=sd' : '/login'),
          text: 'Watch Now',
          target: '_blank'
        }, 'learn_more')
      ]),

      h('div#webinar3.video', [
        h('h3', 'Vendor Management Contracts'),
        webinar_vid(view_model, 'https://videos.sproutvideo.com/embed/189bdab6131deac190/7d9addb3449fd31a?type=sd'),
        h('p.date', 'October 09, 2014'),
        h('p.speaker', 'Speaker: Michael Berman - Founder and CEO - Ncontracts'),
        anchor({
          href: (view_model.show_webinars ? 'https://videos.sproutvideo.com/embed/189bdab6131deac190/7d9addb3449fd31a?type=sd' : '/login'),
          text: 'Watch Now',
          target: '_blank'
        }, 'learn_more')
      ]),

      h('div#webinar4.video', [
        h('h3', 'Vendor Management - Regulatory Expectations for Contracts'),
        webinar_vid(view_model, 'https://videos.sproutvideo.com/embed/7c9bd9bd1611eac5f4/44a6fd89945e027b?type=sd'),
        h('p.date', 'October 09, 2014'),
        h('p.speaker', 'Speaker: Michael Berman - Founder and CEO - Ncontracts'),
        anchor({
          href: (view_model.show_webinars ? 'https://videos.sproutvideo.com/embed/7c9bd9bd1611eac5f4/44a6fd89945e027b?type=sd' : '/login'),
          text: 'Watch Now',
          target: '_blank'
        }, 'learn_more')
      ]),

      h('div#webinar5.video', [
        h('h3', 'RESPA and Joint Marketing'),
        webinar_vid(view_model, 'https://videos.sproutvideo.com/embed/d49bdab5191ae6c45c/f8b7fca9e57a6cf4?type=sd'),
        h('p.date', 'December 11, 2014'),
        h('p.speaker', 'Speaker: Loretta Salzano, Franzén and Salzano'),
        anchor({
          href: (view_model.show_webinars ? 'https://videos.sproutvideo.com/embed/d49bdab5191ae6c45c/f8b7fca9e57a6cf4?type=sd' : '/login'),
          text: 'Watch Now',
          target: '_blank'
        }, 'learn_more')
      ]),

      h('div#webinar6.video', [
        h('h3', 'E-Sign Act: Compliance in Real-World Scenarios'),
        webinar_vid(view_model, 'https://videos.sproutvideo.com/embed/1c9bdab41f1ae2cd94/8635cae49a1ff7b1?type=sd'),
        h('p.date', 'November 13, 2014'),
        h('p.speaker', 'Speaker: Coppelia Padgett, AffirmX'),
        anchor({
          href: (view_model.show_webinars ? 'https://videos.sproutvideo.com/embed/1c9bdab41f1ae2cd94/8635cae49a1ff7b1?type=sd' : '/login'),
          text: 'Watch Now',
          target: '_blank'
        }, 'learn_more')
      ])
    ]),

    h('h3.section_title',
      h('span', 'Popular Webinars')
    ),

    h('section#popular_webinars', [
      h('ul', [
        h('li', [
          h('h3',
            anchor({
              href: 'https://education.cbancnetwork.com/Webinar/WebinarLanding?webinarID=337&utm_medium=weblink&utm_source=CBANCWeb&utm_content=Popular1&utm_campaign=EducationPage',
              text: 'Call Reporting Bootcamp',
              target: '_blank'
            })
          ),
          h('p.speaker', 'Speakers: Mauldin & Jenkins, LLC'),
          h('p.date', 'Monday, March 24th - 1:30pm'),
          anchor({
            href: 'https://education.cbancnetwork.com/Webinar/WebinarLanding?webinarID=337&utm_medium=weblink&utm_source=CBANCWeb&utm_content=Popular1&utm_campaign=EducationPage',
            text: 'Watch Now',
            target: '_blank'
          }, 'webinar')
        ]),
        h('li', [
          h('h3',
            anchor({
              href: 'https://education.cbancnetwork.com/Webinar/WebinarLanding?webinarID=351&utm_medium=weblink&utm_source=CBANCWeb&utm_content=Popular3&utm_campaign=EducationPage',
              text: 'Social Media and Banking Bootcamp',
              target: '_blank'
            })
          ),
          h('p.speaker', "Speakers: Eric Cook and Deborah Gonzalez, DigitalRCP"),
          h('p.date', 'Monday, March 24th - 1:30pm'),
          anchor({
            href: 'https://education.cbancnetwork.com/Webinar/WebinarLanding?webinarID=351&utm_medium=weblink&utm_source=CBANCWeb&utm_content=Popular3&utm_campaign=EducationPage',
            text: 'Watch Now',
            target: '_blank'
          }, 'webinar')
        ]),
        h('li', [
          h('h3',
            anchor({
              href: 'https://education.cbancnetwork.com/Webinar/WebinarLanding?webinarID=386&utm_medium=weblink&utm_source=CBANCWeb&utm_content=Popular2&utm_campaign=EducationPage',
              text: 'Compliance Change Management',
              target: '_blank'
            })
          ),
          h('p.speaker', "Speakers: Eric Cook and Deborah Gonzalez, DigitalRCP"),
          h('p.date', 'Monday, March 24th - 1:30pm'),
          anchor({
            href: 'https://education.cbancnetwork.com/Webinar/WebinarLanding?webinarID=386&utm_medium=weblink&utm_source=CBANCWeb&utm_content=Popular2&utm_campaign=EducationPage',
            text: 'Watch Now',
            target: '_blank'
          }, 'webinar')
        ])
      ])
    ]),

    h('hr'),

    h('section#reserve_your_seat', [
      h('h2', 'Reserve your seat for our live webinars'),
      h('p', 'Content experts from payments and compliance to security and technology'),
      anchor({
        text: 'See Calendar',
        href: 'https://education.cbancnetwork.com/Webinar/BrowseWebinars?utm_medium=weblink&utm_source=CBANCWeb&utm_content=BottomCTA&utm_campaign=EducationPage',
        target: '_blank'
      })
    ])
  ]);
});
