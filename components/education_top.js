var h = require('mercury').h;
var anchor = require('ui_link');

module.exports = function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var learn_more_action = view_model.learn_more_action || {};

	return h('section#hero', [
		h('header', [
			h('h1', 'CBANC Education'),
			h('h2', 'Providing you with the most current and relevant banking web education uaffiliated with any commercial interests.'),
			h('p', [
				anchor({
					text: 'Learn More',
					href: learn_more_action.href || '#',
					target: learn_more_action.target || null
				}, 'learn_more')
			])
		])
	]);
};