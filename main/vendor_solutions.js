var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var assets = require('ui_assets');
var ui_assets = require('ui_assets');
var section = require('../components/content_section.js');

module.exports = versionify(function (state) {
  state = state || {};
  var view_model = state.view_model || {};
  var page_model = state.page_model || {};
  var testimonials = state.view_model.testimonials || {};
  var assets = ui_assets(state);

  return h('article', [

    h('section#sections', [
      section({
        html_class: 'claim',
        dark: false,
        href: '#',
        side: 'right',
        title: 'Claim your free Vendor Page',
        content: h("ul", [
          h("li",
            "Your Vendor Page is the destination for CBANC members to stay up-to-date on your company news, products, and services."
          ),
          h("li",
            "Setting up your page will give you the presence you need inside of the CBANC community to build brand awareness and stay top of mind."
          ),
          h("li",
            "Setting up your free Vendor Page is simple. Click \"Get Started\" and search for your company. Afterwards, verify your association to claim the Vendor Page on your company's behalf."
          )
        ])
      }),

      section({
        html_class: 'compilance',
        dark: true,
        href: '#',
        side: 'left',
        title: 'Describe your products & services',
        content: h("ul", [
          h("li",
            "Your company's presence isn't complete until you add the name, description, and related topics for each of your offerings."
          ),
          h("li",
            "Providing this information will enable CBANC members to find your company and learn more about what you have to offer."
          ),
          h("li",
            "As you build out your Vendor Page, try to be concise with your description, but make sure to include what makes you unique."
          )
        ])
      }),

      section({
        html_class: 'exposure',
        dark: false,
        href: '#',
        side: 'right',
        title: 'Gain exposure to thousands of bank and credit union influencers',
        content: h("ul", [
          h("li",
            "By setting up your company's Vendor Page, you can control your company's presence inside the largest online community of verified financial professionals."
          ),
          h("li",
            "From compliance and deposit operations to retail banking and lending, our community is filled with decision makers inside of banks and credit unions across the United States."
          ),
          h("li",
            "Gaining exposure to these decision makers is simple. Create your Vendor Page today."
          )
        ])
      })
    ]),
  ]);
});
