var h = require('mercury').h;
var versionify = require('../versionify.js');
var ui_assets = require('ui_assets');

module.exports = versionify(function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var assets = ui_assets(state);

	return h('article', [

    h('h3.section_title',
      h('span', 'About Us')
    ),

		h('section#about', [
      h('figure',
      	assets('about/CBANC_office.jpg', {alt: 'CBANC Network Office'})
    	),
			h('p', 'We are the professional network for the banking industry, powering the largest online community of banks and credit unions in the world. Every business day, CBANC helps thousands of verified financial professionals and their institutions make more intelligent vendor decisions, navigate compliance challenges, and answer questions.'),
			h('p', 'Our software leverages the network effects inherent in our community, enabling our members and the vendors that serve them to work together to solve problems. The results are more efficient operations, the ability to better serve customers, and an improved competitive position for our members and the US banking system.')
		]),

    h('h3.section_title',
      h('span', 'Leadership')
    ),

		h('section#leadership', [
			h('div.lead', [
        h('figure',
        	assets('about/bryan_koontz.jpg', {alt: 'Bryan Koontz'})
      	),
				h('div.bio', [
					h('h2', ['Bryan Koontz', h('span', '- Chief Executive Officer')]),
					h('p', "Prior to CBANC, Bryan served as VP and General Manager at Vast.com, Inc., a web search & analytics company powering consumer search solutions for Fortune 500 companies such as Yahoo, AOL, Southwest Airlines and USAA."),
					h('p', "Before joining Vast, Bryan was Senior Vice President and General Manager of the Software Division of Tripos, Inc., a publicly-traded life sciences company, which was later acquired by Vector Capital, a San Francisco-based private equity firm. Bryan previously held roles as vice president of marketing and vice president of corporate development at Tripos.")
				])
			]),
			h('ul.roster', [
				h('li', [
          h('figure',
          	assets('about/tommy_messbauer.jpg', {alt: 'Tommy Messbauer'})
        	),
					h('h2', 'Tommy Messbauer'),
					h('span', 'Chief Technology Officer')
				]),
				h('li', [
          h('figure',
          	assets('about/monica_howeth.jpg', {alt: 'Monica Rios Howeth'})
        	),
					h('h2', 'Monica Rios Howeth'),
					h('span', 'Director of Education')
				]),
				h('li', [
          h('figure',
          	assets('about/julie_smith.jpg', {alt: 'Julie Smith'})
        	),
					h('h2', 'Julie Smith'),
					h('span', 'Director of Member Experience')
				]),
				h('li', [
          h('figure',
          	assets('about/william_griggs.jpg', {alt: 'William Griggs'})
        	),
					h('h2', 'William Griggs'),
					h('span', 'Director of Marketing')
				]),
				h('li', [
          h('figure',
          	assets('about/cara_hayward.jpg', {alt: 'Cara Hayward'})
        	),
					h('h2', 'Cara Hayward'),
					h('span', 'Director of Business Development')
				])
			])
		]),

    h('h3.section_title',
      h('span', 'Board of Directors')
    ),

		h('section#board_of_directors', [
			h('div.lead', [
        h('figure',
        	assets('about/hank_seale.jpg', {alt: 'Hank Seale'})
      	),
				h('div.bio', [
					h('h2', ['R.H. "Hank" Seale, III', h('span', '- Chairman of the Board')]),
					h("p", "Hank Seale is the Founder and Chairman of CBANC. He has experience both as a community banker and as an entrepreneur, building successful software companies that develop innovative financial services solutions as well as provide award-winning customer support."),
					h("p", "An industry veteran, Hank began his career as a loan payments teller for The Bank of the Hills, working his way up to vice president of operations. During that time, he was secretary and treasurer of the Young Bankers Division of the Independent Bankers Association of Texas. Following the acquisition of The Bank of the Hills, Hank co-founded Dallas-based Regency Voice Systems, a company that introduced voice banking to community banks and evolved into a multi-million dollar business with more than 1,600 community bank customers."),
					h("p", "When Regency was acquired by Transaction Systems Architects, Inc. in 1997, Hank went on to start Q UP Systems, a provider of technology to more than 700 community banks. He was a nominee and finalist for Ernst and Young's Entrepreneur of the Year award in 1999. Q UP became part of S1 Corporation in 2000, and he served as the CEO of S1's Community and Regional Solutions group until 2001. In 2004, Hank also founded Q2 Holdings, Inc. (NYSE: QTWO), now a leader in providing virtual banking technology to community and regional banks and credit unions."),
					h("p", "Passionate about the financial services industry, he spends time creating and refining his vision for providing industry-leading technology solutions, along with superior customer service, to the community bank and credit union marketplace."),
					h("p", "Hank is an active entrepreneur and investor in multiple companies. Hank is a graduate of Texas Tech University.")
				])
			]),
			h('ul.roster', [
				h('li', [
          h('figure',
          	assets('about/charles_t_doyle.jpg', {alt: 'Charles T. Doyle'})
        	),
					h('h2', 'Charles T. Doyle'),
					h('span', 'Chairman Emeritus, Texas First Bank')
				]),
				h('li', [
          h('figure',
          	assets('about/mike_j_maples_sr.jpg', {alt: 'Mike J. Maples, Sr.'})
        	),
					h('h2', 'Mike J. Maples, Sr.'),
					h('span', 'Former EVP, Worldwide Products Group, Microsoft Corporation')
				]),
				h('li', [
					h('figure',
						assets('about/jeff_diehl.jpg', {alt: 'Jeff Diehl'})
					),
					h('h2', 'Jeff Diehl'),
					h('span', 'Partner & Head of Investments, Adam Street Partners')
				])
			])
		])
	]);
});
