var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var form = require('ui_forms');
var assets = require('ui_assets');

var download_form = function (user) {
  user = user || {};

  if (user.logged_in) {
    return h('div.download',
      h('form#stonecastle_download', [
        h('h3', 'Download "Unlocking a New Source of Capital for Community Banks" (PDF)'),
        h('p', 'Learn more about the $50B problem and how StoneCastle can help.'),
        h("button", {
          "type": "button"
        }, "Download"),
        h('p.disclaimer', 'By downloading this free guide, I agree to be contacted by StoneCastle.')
      ])
    )
  } else {
    return h('div.download',
      h('form#stonecastle_download', [
        h('h3', 'Download "Unlocking a New Source of Capital for Community Banks" (PDF)'),
        h('p', 'Learn more about the $50B problem and how StoneCastle can help.'),
        h("button", {
          "type": "button"
        }, "Sign In to Download"),
        h('p.disclaimer', 'By downloading this free guide, I agree to be contacted by StoneCastle.')
      ])
    )
  }
};


module.exports = versionify(function (state) {
  state = state || {};
  var view_model = state.view_model || {};
  var page_model = state.page_model || {};

  return h('article', [

    h('h3.section_title',
      h('span', "Looking for a new source of capital?")
    ),

    h('section#help.wrapper', [
      h('article.help', [
        h('p', 'StoneCastle is here to help.'),
        h('p', 'The StoneCastle team has been investing in community banks for more than a decade and is considered one of the largest investors of its kind.'),
        h('p', 'The firm views itself as a long-term partner that understands the needs of community banks. Its flexibility to make investments with a long-term view and reasonable return requirements makes it an ideal investor.'),
        h('p', 'Want to learn more? Download "Unlocking a New Source of Capital for Community Banks" (PDF) to learn about the $50B problem facing community banks and learn how you can go about solving the problem for your bank.')
      ]),
      download_form(state.user)
    ]),

    h('hr'),

    h('section#partners.wrapper', [
      h("span", assets('stonecastle/ibat-logo.jpg', {alt: 'IBAT'})),
      h("span", assets('stonecastle/aba-logo.jpg', {alt: 'ABA'})),
      h("span", assets('stonecastle/sba-logo.jpg', {alt: 'SBA'})),
      h("span", assets('stonecastle/cba-logo.jpg', {alt: 'CBA'})),
      h("span", assets('stonecastle/mba-logo.jpg', {alt: 'MBA'})),
      h("span", assets('stonecastle/kba-logo.jpg', {alt: 'KBA'}))
    ]),

    h('hr'),

    h('section#about_criteria.wrapper', [
      h('article.about', [
        h('h3', 'About StoneCastle'),
        h('p', 'StoneCastle Financial Corp. ("StoneCastle") is the first public investment company specifically established to invest in healthy community banks. StoneCastle invests in community banks that have experienced management teams, stable earnings, sustainable markets and growth opportunities. StoneCastle provides long-term, nonvoting, efficiently priced passive capital for community banks, both publicly and privately held.'),
        h('p', 'If you are considering any of the following: (1) acquisitions, (2) the needed capital to support organic growth, or (3) looking to provide liquidity to shareholders, the StoneCastle team is prepared to review your capital needs and strategic goals at your convenience')
      ]),

      h('article.criteria', [
        h('h3', "StoneCastle's Investment Criteria:"),
        h('p', 'Eligible Banks - Bank holding companies, commercial banks and savings institutions; each typically with less than $10 billion of total assets; public and private entities'),
        h("ul", [
          h("li", ["Instruments",
            h("ul", [
              h("li", "Non-cumulative convertible preferred equity non-cumulative preferred equity, subordinated debt, bank stock loans, convertible trust preferred securities, warrants and, to a lesser extent, common equity"),
              h("li", "Quarterly Dividend or Interest Payments")
            ])
          ]),
          h("li", "Typical Investment Size: $2 - $15 million, smaller or larger by exception, with maximum pro-forma ownership limited to 24.9%"),
          h("li", "Investment Process Timeframe: Typically 3 - 6 weeks; variable by institution and structure")
        ])
      ])
    ]),

    h('hr'),

    h('section#talk.wrapper', [
      h('h2', 'Talk with StoneCastle'),
      h('p', 'Fill out the form below and representatives from StoneCastle will be in touch shortly.'),
      h('form', [
        form("text", {
          name: "name",
          placeholder: "Name"
        }),
        form("text", {
          name: "email",
          placeholder: "Work Email Address"
        }),
        form("text", {
          name: "phone",
          placeholder: "Phone"
        }),
        h("button", {
          "type": "button"
        }, "Contact StoneCastle")
      ])
    ])
  ]);
});
