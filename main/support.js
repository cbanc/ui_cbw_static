var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var signature = require('ui_signature/components/public.js');
var ui_assets = require('ui_assets');

var sig_check = function (testimonial) {
	if (testimonial) {
		var org = testimonial.org;
		var created_date = testimonial.created_date;

		return signature(testimonial, org, created_date, 'Quoted')
	}
	return h('p.cbanc_member', 'Julie Smith - Director of Member Experience at CBANC Network');
}

module.exports = versionify(function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var assets = ui_assets(state);

	return h('article', [

		h('section.top', [
			h('div#contact', [
				h('h1', 'Contact Us'),
				h('p', 'Do you have an issue or want to let us know what you think?'),
				h('h3', [
					h('i.fa.fa-phone'),
					' 512.583.4570 or '
				]),
				anchor({
					href: 'https://cbanc.zendesk.com/hc/en-us/requests/new',
					text: 'Email Support',
					target: '_blank'
				})
			]),
			h('div#faq', [
				h('h1', 'FAQ'),
				h('p', 'You can also find answers to common questions on our FAQ page.'),
				anchor({
					href: 'https://cbanc.zendesk.com/hc/en-us/categories/200157594-FAQ',
					text: 'CBANC FAQ',
          target: '_blank'
				})
			])
		]),

    h('section#quote_cta',
      h('div.wrapper',
        h('article.quote', [

          h('div.avatar', [
            h('figure', [
              assets('about/julie_smith.jpg', {alt: 'Julie Smith - Director of Member Experience'}),
            ]),
          ]),
          h ('div.details', [
            h('p', '"We pride ourselves in offering amazing service to our members. If you have any questions, problems, or concerns please don\'t hesitate to reach out!"'),
            sig_check(view_model.julie),
          ]),
        ])
      )
    ),

		h('section#location', [
			h('h1', 'Office Location'),
			h('p.address', [
				h('span', '4200 N Lamar Blvd Suite 250'),
				h('span', 'Austin, TX 78756')
			]),
			h("iframe", {
				"src": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3444.415964479333!2d-97.74003800000001!3d30.310684000000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644ca62cb940f5d%3A0xeb709d3636e054e8!2s4200+N+Lamar+Blvd+%23250%2C+Austin%2C+TX+78756%2C+USA!5e0!3m2!1sen!2srs!4v1413885232221",
				"frameborder": "0"
			})
		])
	]);
});
