var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var lb_list = require('../components/leaderboard_list.js')

module.exports = versionify(function (state) {
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var leaderboard_list = view_model.leaderboard || [];
	var current_user = view_model.current_user || {};

	return h('section', [
		h('article.leaderboard', [
			h('h2', 'Where do you rank?'),
			h('p', 'The Reputation Leaderboard lists CBANC Members in order of their Reputation Score. Where do you rank?'),
			lb_list(leaderboard_list)
		]),
		h('aside.your_rep', [
			h('h4', 'Your Reputation'),
			h('p', 'Reputation Score: ' + (current_user.reputation_score || current_user.reputation_score === 0 ? current_user.reputation_score.toString() : 'N/A')),
			h('p', 'Reputation Rank: In Top ' + (current_user.reputation_rank || current_user.reputation_rank === 0 ? current_user.reputation_rank.toString() + '% of CBANC Members' : 'N/A')),
			h('p', 'Reputation Trend: Moved ' + (current_user.reputation_trend || current_user.reputation_trend === 0 ? current_user.reputation_trend.toString() + ' spots in the last week' : 'N/A'))
		])
	])
});