var httpContext = {
	controller: {
		name: 'static_support'
	}
};

module.exports = {
	view_model: {
		julie: {
			id: "7d52f9f2-1eb6-4f49-a520-be98a8f71ca9",
			updated_date: "2015-09-03T21:47:03.016Z",
			access_token: "MDdkODAyNzMtMGQxOS00NjI1LWIyMTAtZDhlMGE0ZDIwY2I3",
			active: true,
			created_by: "7d52f9f2-1eb6-4f49-a520-be98a8f71ca9",
			created_date: "2009-07-01T00:00:00.000Z",
			email: "jsmith@cbancnetwork.com",
			email_preferences: {
				daily_digest: true,
				documents: false,
				marketing: true,
				qna: false,
				reviews: false,
				synchronizer_token: true,
				webinars: false,
				weekly_digest: false
			},
			first_name: "Julie",
			last_name: "Smith",
			legacy_user_id: "749",
			phone: [
				"5125834575"
			],
			publish_email: true,
			publish_phone: true,
			title: "Director of Member Experience",
			updated_by: "7d52f9f2-1eb6-4f49-a520-be98a8f71ca9",
			years_banking_experience: 0,
			_type: "person",
			full_name: "Julie Smith",
			password: null,
			org: {
				id: "bcee7945089f13b76ce5d9b9e59d5a73",
				updated_date: "2015-02-19T18:45:05.730Z",
				active: true,
				address: [
					"4200 N Lamar Blvd Ste 250"
				],
				asset_size: 0,
				branches: 0,
				city: "Austin",
				country: "US",
				created_by: "00000000-0000-0000-0000-000000000000",
				created_date: "2014-10-21T21:31:47.711Z",
				display_name: "CBANC Network",
				domain: [
					"cbancnetwork.com"
				],
				fi_type: "bank",
				legal_name: "CBANC Network",
				phone: "5125834570",
				postal_code: "78756",
				regulatory_agencies: [
					"FDIC"
				],
				routing_number: "CBANC",
				state: "TX",
				updated_by: "00000000-0000-0000-0000-000000000000",
				website: "http://www.cbancnetwork.com",
				_type: "fi_org"
			}
		}
	},
	httpContext: httpContext,
	config: require('../../tmp/js/code_gen/config.js'),
	page_model: {
		title: 'CBANC Support',
		stylesheets: [
			'./index.css'
		],
		page_title: 'Support',
		breadcrumbs: [{
			text: 'Home',
			href: '/'
		}, {
			text: 'Support',
			href: '#'
		}],
		header: {
			mobile_menu_expanded: false,
			mobile_user_menu_expanded: false,
			menu_items: [{
				text: "Documents",
				href: "/documents"
			}, {
				text: "Q&A",
				href: "/questions"
			}, {
				text: "Products",
				href: "/products"
			}, {
				text: "Education",
				href: "/education"
			}, {
				text: "Vendor Management",
				href: "/vendormanagement"
			}],
			sign_in: {
				text: "Sign In",
				href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
			},
			sign_out: {
				text: "Sign Out",
				href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
			},
			join: {
				text: "Join For Free",
				href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
			}
		},
	}
};