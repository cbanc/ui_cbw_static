var httpContext = {
	controller: {
		name: 'static_terms_of_service'
	}
};

module.exports = {
	view_model: {},
	httpContext: httpContext,
	config: require('../../tmp/js/code_gen/config.js'),
	page_model: {
		title: 'Terms of Service',
		stylesheets: [
			'./index.css'
		],
		page_title: "Terms of Service",
		breadcrumbs: [{
			text: 'Home',
			href: '/'
		}, {
			text: 'Support',
			href: '#'
		}, {
			text: 'Terms of Service',
			href: '#'
		}],
		header: {
			mobile_menu_expanded: false,
			mobile_user_menu_expanded: false,
			menu_items: [{
				text: "Documents",
				href: "/documents"
			}, {
				text: "Q&A",
				href: "/questions"
			}, {
				text: "Products",
				href: "/products"
			}, {
				text: "Education",
				href: "/education"
			}, {
				text: "Vendor Management",
				href: "/vendormanagement"
			}],
			sign_in: {
				text: "Sign In",
				href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
			},
			sign_out: {
				text: "Sign Out",
				href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
			},
			join: {
				text: "Join For Free",
				href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
			}
		},
	}
};