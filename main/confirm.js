var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');

module.exports = versionify(function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};

	return h('div.confirm', [
		h('h1', 'Thanks for your interest!'),
		h('h3', [
			'A member of our team will be in touch soon to set up your demo. Until then, dive in to all the great content on the CBANC Network. Check out helpful ',
			anchor({
				text: 'Documents',
				href: '/documents'
			}),
			' or ',
			anchor({
				text: 'Q&A',
				href: '/questions'
			}),
			'.'
		])
	]);
});