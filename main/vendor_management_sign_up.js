var h = require('mercury').h;
var anchor = require('ui_link');
var versionify = require('../versionify.js');

var infusion_input = function (_attributes) {
  return h('input', {
    attributes: _attributes
  })
};

var infusionsoft_form = function (view_model) {
  var infusionsoft = view_model.infusionsoft || {};
  var person = view_model.person || false;
  if (!person) return null;

  return h('form.infusion-form', {
    attributes: {
      action: infusionsoft.form_action,
      method: "POST"
    }
  }, [
    h('h3', "Easy Sign Up"),
    infusion_input({
      name: "inf_form_xid",
      type: "hidden",
      value: "142389fc50cae690633082e8b0a78880"
    }),
    infusion_input({
      name: "inf_form_name",
      type: "hidden",
      value: "Prospective Vendor"
    }),
    infusion_input({
      name: "infusionsoft_version",
      type: "hidden",
      value: "1.43.0.43"
    }),
    h('div.infusion-field', [
      h('label', [
        "First Name *",
        h('input.infusion-field-input-container', {
          attributes: {
            id: 'inf_field_FirstName',
            name: 'inf_field_FirstName',
            type: 'text',
            required: '',
            value: person.first_name
          }
        })
      ])
    ]),
    h('div.infusion-field', [
      h('label', [
        "Last Name *",
        h('input.infusion-field-input-container', {
          attributes: {
            id: 'inf_field_LastName',
            name: 'inf_field_LastName',
            type: 'text',
            required: '',
            value: person.last_name
          }
        })
      ])
    ]),
    h('div.infusion-field', [
      h('label', [
        "Work Email *",
        h('input.infusion-field-input-container', {
          attributes: {
            id: 'inf_field_Email',
            name: 'inf_field_Email',
            type: 'email',
            required: '',
            value: person.email
          }
        })
      ])
    ]),
    h('div.infusion-field', [
      h('label', [
        "Phone *",
        h('input.infusion-field-input-container', {
          attributes: {
            id: 'inf_field_Phone1',
            name: 'inf_field_Phone1',
            type: 'text',
            required: '',
            pattern: "^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$",
            value: person.phone
          }
        })
      ])
    ]),
    h('div.infusion-submit', [
      h('input', {
        attributes: {
          value: 'Request Demo',
          type: 'submit'
        }
      })
    ])
  ]);
};


module.exports = versionify(function (state) {
  state = state || {};
  var infusionsoft = state.view_model.infusionsoft || {};
  if (!infusionsoft) return null;

  return h('article#vm_public', [
    h('div.main_content', [
      h('h1', 'Start Your Free Demo Today!'),
      h('p', "Vendor Management from CBANC is a dynamic risk management tool you can use to securely monitor & track your vendors - simplifying your compliance management.  Please fill out the short form below and we will get in touch to schedule your free demo & learn more about your vendor management software needs."),
      infusionsoft_form(state.view_model || {}),
      h('script', {
        attributes: {
          type: 'text/javascript',
          src: infusionsoft.tracking_script
        }
      }),
      h('img', {
        attributes: {
          src: state.view_model.screenshot_img_url
        }
      }),
      h('hr'),
      h('h3', [
        anchor({
          href: state.view_model.vendor_management_home_link,
          icon: 'arrow-circle-left',
          text: 'Learn More About CBANC Vendor Management'
        })
      ])
    ])
  ]);
});