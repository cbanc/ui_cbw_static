var httpContext = {
	controller: {
		name: 'static_reputation_leaderboard'
	}
};

module.exports = {
	view_model: {
		"current_user": {
			"reputation_trend": 0,
			"reputation_rank": 100,
			"reputation_score": 5,
			"email_marketing": true,
			"title": "Step Brother",
			"years_banking_experience": 6,
			"functional_level": "VP",
			"functional_area": "Vacationer",
			"phone": ["6769889876"],
			"active": true,
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": false,
			"publish_email": true,
			"publish_phone": false,
			"email": "ferb@fletcher.com",
			"last_name": "Fletcher",
			"first_name": "Ferb",
			"key": "93983478-5687-47d6-b11c-cf1fb186e86a",
			"id": 2629,
			"orgs": [],
			"reputation": {
				"thanks_count": 8
			},
			"questions": {
				"results": [{
					"document_request": true,
					"anonymous": false,
					"text": "Is this who is this is supposed be?",
					"title": "Who is this is?",
					"active": true,
					"key": "36ccfc01-7b49-4cf3-a5c6-935520732f3a",
					"id": 2697
				}]
			},
			"documents": {
				"results": [{
					"examtested": false,
					"synopsis": "The autobiographical tyrate of Monty Burns.",
					"file_extension": "Welcome to the Club",
					"title": "How to Succeed In Business",
					"active": true,
					"key": "1082ef71-c838-4697-8913-3e02f49950cb",
					"id": 2680
				}, {
					"examtested": false,
					"synopsis": "One thing the fore-father's of Springfield loved was their booze.",
					"file_extension": "Doh!",
					"title": "Founder's Day Prohibition Flyer",
					"active": true,
					"key": "3d8fcf4f-2c93-4ed8-bc58-bf6c283c0fdf",
					"id": 2683
				}]
			}
		},
		"leaderboard": [{
			"reputation_trend": 0,
			"reputation_rank": 100,
			"reputation_score": 5,
			"email_marketing": true,
			"password_salt": "$2a$10$Ksb.Ej2TZt14GaO1Sl/ZP.",
			"title": "Step Brother",
			"years_banking_experience": 6,
			"functional_level": "VP",
			"functional_area": "Vacationer",
			"phone": ["6769889876"],
			"active": true,
			"password": "ce557e0e839513eec090265aff0d3637d4c5a3ba3421740f0c742a1c5d3f47db95e9d7545a169d8a6627048b7b908e07aa5f9a8f47f781b949e71f196c9f28a5",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": false,
			"publish_email": true,
			"publish_phone": false,
			"email": "ferb@fletcher.com",
			"last_name": "Fletcher",
			"first_name": "Ferb",
			"key": "93983478-5687-47d6-b11c-cf1fb186e86a",
			"id": 2629
		}, {
			"reputation_trend": -5,
			"reputation_rank": 95,
			"reputation_score": 4,
			"email_marketing": true,
			"password_salt": "$2a$10$yYozTrR70HAbzVFefeIGDe",
			"title": "Religious Neighbor",
			"years_banking_experience": 9,
			"functional_level": "VP",
			"functional_area": "Mustache",
			"phone": ["7777771234"],
			"active": true,
			"password": "8cc02a9ab1e53a55b0b5c26b270a5306fe6811a44d762efd6c40b327965c1e36121c22efddd8e5d4828b19751a91cfcfd2285952717f75a526500e78e9d87207",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": false,
			"publish_email": true,
			"publish_phone": false,
			"email": "ned@flanders.com",
			"last_name": "Flanders",
			"first_name": "Ned",
			"key": "957110f5-0c74-4ddb-be98-21f8a4e1b3e0",
			"id": 2618
		}, {
			"reputation_trend": -9,
			"reputation_rank": 91,
			"reputation_score": 1,
			"email_marketing": true,
			"password_salt": "$2a$10$J2tP4kzCahIaIK.G4KF9r.",
			"title": "Dad",
			"years_banking_experience": 3,
			"functional_level": "CEO",
			"functional_area": "Donuts",
			"phone": ["5555551234"],
			"active": true,
			"password": "26d4a4a60c80e127339f23209155c1ce2901520485cf50a0e6ecabc718a8a39258aef6b68b3d4eb9b50efb7b85431a91dc90fc9d0a3d6e81e8d71a90153cca3d",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": false,
			"publish_phone": true,
			"email": "homer@simpson.com",
			"last_name": "Simpson",
			"first_name": "Homer",
			"key": "6815cfb1-ca95-4611-ad1b-9f2cb41843ab",
			"id": 2614
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"password": "",
			"email_documents": false,
			"email_upcoming": false,
			"email_reviews": false,
			"email_qna": false,
			"email_daily_digest": false,
			"publish_email": false,
			"publish_phone": false,
			"email": "bot@cbancnetwork.com",
			"last_name": "CBANC Bot",
			"first_name": "CBANC Bot",
			"key": "00000000-0000-0000-0000-000000000000",
			"id": 2
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": false,
			"password_salt": "$2a$10$WdMgMj/OoAfWtw8zbOc79u",
			"title": "Jake the Dog",
			"years_banking_experience": 34,
			"functional_level": "CFO",
			"functional_area": "Criminal",
			"phone": [],
			"active": true,
			"password": "202483faa4ae14dd0b46aa11afc356fec3854ca08638bb43cc5128692fa798545f88172737c8c3e0cb34068c89958b1bf1fef823fe898ab51f8c0baf48b6ab41",
			"email_documents": false,
			"email_reviews": false,
			"email_qna": false,
			"email_daily_digest": false,
			"publish_email": false,
			"publish_phone": false,
			"email": "jake@adventuretime.com",
			"last_name": "",
			"first_name": "Jake",
			"key": "d5c35c72-445a-4c77-9cb4-de2ea7ec62f8",
			"id": 2612
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$EKhU.QPJEl2asu.m4ndD7O",
			"title": "Ex-Ruler of the Candy Kingdom",
			"years_banking_experience": 19,
			"functional_level": "Princess",
			"functional_area": "Candy Person",
			"email_secondary": "bonnibel.bubblegum@adventuretime.com",
			"phone": ["9723948374", "1111111111", "2222222222", "3333333333"],
			"active": true,
			"password": "4ed2ab7d5db31bc0208ef7486aa95e05694358fefdb763432e1ddf58a742a2084e3c13716b720f099ecc12fc3e39c10099de5f3d4ee921d540ad4b14c3642f42",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": false,
			"publish_phone": false,
			"email": "bonnibel@adventuretime.com",
			"last_name": "Bubblegum",
			"first_name": "Bonnibel",
			"key": "aba425a0-bfff-43f6-bab2-d84e0e4a9034",
			"id": 2613
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$rWR0ZGRwMeEzLxnvgvY39e",
			"title": "Lumpy Space Princess",
			"years_banking_experience": 19,
			"functional_level": "Princess",
			"functional_area": "Lumpy Space",
			"email_secondary": "lsp@adventuretime.com",
			"phone": ["8088088080", "0000000000"],
			"active": true,
			"password": "0543eae8b43c6443f98b96b8c87bd01ec3b03fa2096fd92ba290ce8e196ecb1a4d29ff7247db9ced6d6251c70eba795f8e0523c50ec6996df6c600c0b699d720",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": true,
			"publish_phone": true,
			"email": "lumpyspaceprincess@adventuretime.com",
			"last_name": "Space Princess",
			"first_name": "Lumpy",
			"key": "4428adc8-7722-47b9-8629-5772677b681e",
			"id": 2615
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$AtOOa.KZZoHS1Wm4kNEdbu",
			"title": "Bully",
			"years_banking_experience": 7,
			"functional_level": "Boss",
			"functional_area": "Bully",
			"phone": [],
			"active": true,
			"password": "cd499d1af3e5b3e57751c095f10fb490d00805d17eef2c92bcbd5f0dd5ce11b68ef26d2e0035147d9cdea3390936eafa3fb480ecdba0f11cf0465073d6c9e10c",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": true,
			"publish_phone": true,
			"email": "nelson@muntz.com",
			"last_name": "Muntz",
			"first_name": "Nelson",
			"key": "6fd60ec9-e563-4a3a-a341-bcea75ee0155",
			"id": 2616
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": false,
			"password_salt": "$2a$10$GFljqw17qr1Zd16pFPrS1e",
			"title": "Queen of the Underworld",
			"years_banking_experience": 20,
			"functional_level": "Queen",
			"functional_area": "Vampire",
			"phone": ["2148374064"],
			"active": true,
			"password": "922967dfe529c3f5b24f4df3679352e926ddc27fd33bbeab6b85596786cbcf2949458e66d82dcb1d9084db7f39ba714645187a7c43769871735de74d74b71294",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": false,
			"publish_email": true,
			"publish_phone": true,
			"email": "marceline@adventuretime.com",
			"last_name": "Abadeer",
			"first_name": "Marceline",
			"key": "48a4910b-4e0b-44ac-aadf-66d71a635ba1",
			"id": 2617
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": false,
			"password_salt": "$2a$10$7IB4Ds4uZobJo0WOJ1Mz0.",
			"title": "Mr.",
			"legacy_user_id": "cd236482-c06c-4851-be27-0ceabcc81f12",
			"years_banking_experience": 7,
			"functional_level": "Master",
			"functional_area": "Jedi",
			"email_secondary": "hydration_station@tatoonie.com",
			"phone": ["6102342132"],
			"active": true,
			"password": "0cba3724c2965b4d6e092d487225c2c1b4ee15483bcece81a065a2c38cc2edc7bd2daa8569fdd64621c5f5cb250b6775d265daff0477ac1e15918e6e20bc2b62",
			"email_documents": true,
			"email_reviews": false,
			"email_qna": false,
			"email_daily_digest": true,
			"publish_email": false,
			"publish_phone": true,
			"email": "lskywalker@starwards.com",
			"last_name": "Sciwalkr",
			"first_name": "Lucas",
			"key": "f3ac67d8-9fa9-473a-8663-8b4be5abd487",
			"id": 2619
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$gG4BfeDmAvcubZz.3nrmv.",
			"title": "Fin the Human",
			"years_banking_experience": 16,
			"functional_level": "CEO",
			"functional_area": "Hero",
			"email_secondary": "finn.mertens@adventuretime.com",
			"phone": ["2148573048"],
			"active": true,
			"password": "cd1db6aa0f62f45a38c350867b05e4751e2df57579efcd795c7489539ff0f172f5bd8401eef07675636290ce1d3c00a500b161aa24fa7154815ea0937058ea0e",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": true,
			"publish_phone": true,
			"email": "finn@adventuretime.com",
			"last_name": "Mertens",
			"first_name": "Finn",
			"key": "3c660093-1b34-456e-bfc8-f7cebbe37c96",
			"id": 2620
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": false,
			"password_salt": "$2a$10$KPLTZz7W7TVAyerKR8qINO",
			"title": "Clerk",
			"years_banking_experience": 1,
			"functional_level": "Director",
			"functional_area": "Cashier",
			"phone": [],
			"active": true,
			"password": "0ded8144540cb170a8345527a61d1f58f4e2b74404108b8fa410371c4844e11b9cdab9900196990a45848d1dc902610bc19f3b1d15cf3a57c62ef42517999314",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": true,
			"publish_phone": true,
			"email": "apu@apu.com",
			"last_name": "Nahasapeemapetilon",
			"first_name": "Apu",
			"key": "91222dc3-c23c-4769-921f-740ea800c60a",
			"id": 2621
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$2RBmrP/gk5P.VMKgBjeYNe",
			"title": "Brother",
			"years_banking_experience": 8,
			"functional_level": "President",
			"functional_area": "SummerCoordinator",
			"phone": ["5125859876"],
			"active": true,
			"password": "99950fc8e717e0ea7c15e75dcd1e3ceb3b8f514aadd3340ea3b6500ff84649f3723f313f53d6a40e91059c0c781b9afdc6c905d0342801992f90427662b01baf",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": false,
			"publish_phone": true,
			"email": "phineas@flynn.com",
			"last_name": "Flynn",
			"first_name": "Phineas",
			"key": "e8516da9-0dc9-42a7-8974-405d6645d298",
			"id": 2622
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$9DzXufixk3Kx5cH54OIVLe",
			"title": "Teacher",
			"years_banking_experience": 9,
			"functional_level": "Coordinator",
			"functional_area": "Teaching",
			"phone": [],
			"active": true,
			"password": "9079aae7c1bd41b8b7d3da2d643caeef77a175e6f783fdf5ef65ac6cd51d16c954b86b8550f025a65eda1c8019fa6c8ebc8c5f9c5ad5c4f818f0d0748e763f83",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": true,
			"publish_phone": true,
			"email": "edna@krabappel.com",
			"last_name": "Krabappel",
			"first_name": "Edna",
			"key": "27c3020d-8281-4f3a-a8ac-a644b67f5af3",
			"id": 2623
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$8CMd0K1lR0dHOxQK2EdLce",
			"title": "Major",
			"years_banking_experience": 18,
			"functional_level": "Major",
			"functional_area": "Military",
			"phone": [],
			"active": true,
			"password": "830fcb8e129b92a06ac0dacb9c6c5cdd2fa107dfcf746bf40046fc47786bcfb193d5dc0b1112b102d262c0f454e0278eab5aefcf5a4392b12ac00cba66e7d2f9",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": true,
			"publish_phone": true,
			"email": "major@monogram.com",
			"last_name": "Monogram",
			"first_name": "Major Francis",
			"key": "8dc4f659-c2b9-40d9-b27f-d81a71aae344",
			"id": 2624
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": false,
			"password_salt": "$2a$10$otCKcB7Onatwhchh4BsWYe",
			"title": "Sister",
			"years_banking_experience": 1,
			"functional_level": "PoliceWoman",
			"functional_area": "FreakingOut",
			"phone": [],
			"active": true,
			"password": "3f7c4a4f2be6cd0db73cfd0e3c3f368e6e8d9f6f0569ddcc2a7ed950a26bf1f20ac13f8eb6eff9122ba2824600847ed66bef0ab36e88de939f187f226ac3f7cd",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": true,
			"publish_phone": true,
			"email": "candice@flynn.com",
			"last_name": "Flynn",
			"first_name": "Candice",
			"key": "be428e49-03f0-45c5-a106-0794934567fa",
			"id": 2625
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$TW2G4C4LjExJjFU40fE3ou",
			"title": "Sr.",
			"legacy_user_id": "3a531292-8a21-4c0a-ac2c-89f87c61c1f4",
			"years_banking_experience": 1,
			"functional_level": "Senior Officier",
			"functional_area": "Loans",
			"email_secondary": "papachecks@google.com",
			"phone": ["2342432111"],
			"active": true,
			"password": "1c7abbc280dd6ee7f20df9e8570bf61b03bb08a69fbbe3f2ae715240bb058609235b22b295439160607f3b7619174177792fe3cf9a1c6ce957a0bcb0fb2db577",
			"email_documents": false,
			"email_reviews": false,
			"email_qna": true,
			"email_daily_digest": false,
			"publish_email": false,
			"publish_phone": true,
			"email": "checkie@jokes.com",
			"last_name": "Eckenroth",
			"first_name": "Aaron",
			"key": "3589e069-b477-4ee3-baa6-041f501157aa",
			"id": 2626
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": false,
			"password_salt": "$2a$10$9w14ExaamI3q5HV3l0cRgO",
			"title": "Mr.",
			"legacy_user_id": "581f719c-58fb-4daa-a776-3de1a384b53e",
			"years_banking_experience": 12,
			"functional_level": "Chief",
			"functional_area": "Marketing",
			"email_secondary": "w.san.salvador@google.com",
			"phone": ["6108932132"],
			"active": true,
			"password": "62a9fdc6e2fcf596d9d8e3afec3e26d6dec0129623fff3521e640462197b0078bc81c578e36d903dd9825a7ab9c5c13d9ef2b8e4dbb6b28727ad6cd9a7f37a1c",
			"email_documents": false,
			"email_reviews": true,
			"email_qna": false,
			"email_daily_digest": false,
			"publish_email": false,
			"publish_phone": true,
			"email": "willysan@hotmail.com",
			"last_name": "SanSalvador",
			"first_name": "Willy",
			"key": "f00838ca-e72a-465d-8a8d-56a40de0aa3a",
			"id": 2627
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": false,
			"password_salt": "$2a$10$1abfWey38pCYiD48cXZ48O",
			"title": "Mr.",
			"legacy_user_id": "4e7e0584-6693-433c-99b6-1d7146f264c2",
			"years_banking_experience": 4,
			"functional_level": "VP",
			"functional_area": "Compliance",
			"email_secondary": "mcneeds@boji.com",
			"phone": ["5122342132"],
			"active": true,
			"password": "89b41d5214e572b124b4185256bd9632b344c1d030f0c9b5b9075c5f7c78cf7e2f03692d8ce3f8fa82089eeb17274631c76f515d558f99fe30282556947e7556",
			"email_documents": false,
			"email_reviews": true,
			"email_qna": false,
			"email_daily_digest": false,
			"publish_email": false,
			"publish_phone": true,
			"email": "boji@mcneeds.com",
			"last_name": "McNeedles",
			"first_name": "Bojangles",
			"key": "9577ea03-e1e5-49d9-bd1c-3122329bde52",
			"id": 2628
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"password_salt": "$2a$10$wCV49RgdTYNLyfhSEa4I.O",
			"title": "CrazyScientist",
			"years_banking_experience": 28,
			"functional_level": "PHD",
			"functional_area": "Terrorist",
			"phone": [],
			"active": true,
			"password": "516475747840748ee92f612655783da2767d9e69b8b23174490228fcef7e4a8bc3a7154b690448ef0cc57db7887ec9a439bbdb9d725e0ee29d46dce48d353094",
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": true,
			"publish_phone": true,
			"email": "dr@doofenshmirtz.com",
			"last_name": "Doofenshmirtz",
			"first_name": "Dr. Heinz",
			"key": "2cfd8d4c-7f46-4d28-8dc4-5add0e38b2fc",
			"id": 2630
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": false,
			"password_salt": "$2a$10$2Yd7jCNDQC48DZkG4UpOjO",
			"title": "Mr.",
			"legacy_user_id": "da4352d3-1ec8-41a1-b4b0-2d3b01814d55",
			"years_banking_experience": 9,
			"functional_level": "Director",
			"functional_area": "Sales",
			"email_secondary": "sipo@gmail.com",
			"phone": ["1122342342"],
			"active": true,
			"password": "430f23d114c816235a336c20134051922563b8a8b0170d9b554aa0d771af5e4bd45068da14d66340bf39ba3a141855e3d9a8df50d7dd9681855b913798d5dc47",
			"email_documents": false,
			"email_reviews": true,
			"email_qna": false,
			"email_daily_digest": false,
			"publish_email": false,
			"publish_phone": true,
			"email": "sport@folio.com",
			"last_name": "Portiea",
			"first_name": "Simon",
			"key": "508542de-332c-4f9c-9f1c-b735a611b049",
			"id": 2631
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"key": "3a531292-8a21-4c0a-ac2c-89f87c61c1f4",
			"id": 2661
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"email_marketing": true,
			"title": "",
			"functional_level": "",
			"functional_area": "",
			"phone": ["5555554444"],
			"active": false,
			"email_documents": true,
			"email_reviews": true,
			"email_qna": true,
			"email_daily_digest": true,
			"publish_email": false,
			"publish_phone": false,
			"email": "bill@partytime.com",
			"last_name": "Murray",
			"first_name": "Bill",
			"key": "be672c25-fbc7-4536-b870-206c0712a544",
			"id": 2720
		}, {
			"reputation_trend": -13,
			"reputation_rank": 87,
			"reputation_score": 0,
			"password": "",
			"email_documents": false,
			"email_upcoming": false,
			"email_reviews": false,
			"email_qna": false,
			"email_daily_digest": false,
			"publish_email": false,
			"publish_phone": false,
			"email": "everyone_bot@cbancnetwork.com",
			"last_name": "Everyone Bot",
			"first_name": "Everyone Bot",
			"key": "00000000-0000-0000-0000-000000000001",
			"id": 2726
		}]
	},
	httpContext: httpContext,
	config: require('../../tmp/js/code_gen/config.js'),
	page_model: {
		title: 'CBANC\'s Reputation Leaderboard',
		stylesheets: [
			'./index.css'
		],
		page_title: "CBANC\'s Reputation Leaderboard",
		breadcrumbs: [{
			text: 'Home',
			href: '/'
		}, {
			text: 'Reputation',
			href: '/reputation'
		}, {
			text: 'Reputation Leaderboard',
			href: '/reputation/leaderboard'
		}],
		header: {
			mobile_menu_expanded: false,
			mobile_user_menu_expanded: false,
			menu_items: [{
				text: "Documents",
				href: "/documents"
			}, {
				text: "Q&A",
				href: "/questions"
			}, {
				text: "Products",
				href: "/products"
			}, {
				text: "Education",
				href: "/education"
			}, {
				text: "Vendor Management",
				href: "/vendormanagement"
			}],
			sign_in: {
				text: "Sign In",
				href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
			},
			sign_out: {
				text: "Sign Out",
				href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
			},
			join: {
				text: "Join For Free",
				href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
			}
		},
	}
};