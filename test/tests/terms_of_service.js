var toHTML = require('vdom-to-html');
var assert = require('assert');
var _ = require('lodash');

var data = require('../fixture/terms_of_service.js');

// ui_page needs a page table
var render = require('ui_page')(require('../../main/terms_of_service.js'));


suite('TOS Page Render', function () {

	test('TOS Page Render', function () {
		var dom;

		try {
			dom = render(data);
		} catch (e) {
			assert.ifError(e);
		}

		console.log(toHTML(dom));
	});

});