var httpContext = {
	controller: {
		name: 'static_vendor_management'
	}
};

module.exports = {
	view_model: {
		cara: {
			id: "7d52f9f2-1eb6-4f49-a520-be98a8f71ca9",
			updated_date: "2015-09-03T21:47:03.016Z",
			access_token: "MDdkODAyNzMtMGQxOS00NjI1LWIyMTAtZDhlMGE0ZDIwY2I3",
			active: true,
			created_by: "7d52f9f2-1eb6-4f49-a520-be98a8f71ca9",
			created_date: "2009-07-01T00:00:00.000Z",
			email: "chayward@cbancnetwork.com",
			email_preferences: {
				daily_digest: true,
				documents: false,
				marketing: true,
				qna: false,
				reviews: false,
				synchronizer_token: true,
				webinars: false,
				weekly_digest: false
			},
			first_name: "Cara",
			last_name: "Hayward",
			legacy_user_id: "749",
			phone: [
				"5125834575"
			],
			publish_email: true,
			publish_phone: true,
			title: "Director of Business Development",
			updated_by: "7d52f9f2-1eb6-4f49-a520-be98a8f71ca9",
			years_banking_experience: 0,
			_type: "person",
			full_name: "Cara Hayward",
			password: null,
			org: {
				id: "bcee7945089f13b76ce5d9b9e59d5a73",
				updated_date: "2015-02-19T18:45:05.730Z",
				active: true,
				address: [
					"4200 N Lamar Blvd Ste 250"
				],
				asset_size: 0,
				branches: 0,
				city: "Austin",
				country: "US",
				created_by: "00000000-0000-0000-0000-000000000000",
				created_date: "2014-10-21T21:31:47.711Z",
				display_name: "CBANC Network",
				domain: [
					"cbancnetwork.com"
				],
				fi_type: "bank",
				legal_name: "CBANC Network",
				phone: "5125834570",
				postal_code: "78756",
				regulatory_agencies: [
					"FDIC"
				],
				routing_number: "CBANC",
				state: "TX",
				updated_by: "00000000-0000-0000-0000-000000000000",
				website: "http://www.cbancnetwork.com",
				_type: "fi_org"
			}
		},
		testimonials: [{
			id: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
			updated_date: "2015-01-21T21:29:14.806Z",
			access_token: null,
			active: true,
			created_by: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
			created_date: "05/22/2015",
			email: "tami.benedict@callawaybank.com",
			email_preferences: {
				daily_digest: true,
				documents: true,
				qna: true,
				reviews: true,
				webinars: true
			},
			first_name: "Tami",
			last_name: "Benedict",
			legacy_user_id: "7651",
			phone: [
				"5735926378"
			],
			publish_email: false,
			publish_phone: false,
			title: "Vice President, Operations Manager",
			updated_by: "00000000-0000-0000-0000-000000000000",
			years_banking_experience: 0,
			_type: "person",
			full_name: "Tami Benedict",
			password: null,
			org: {
				id: "6e811a1f66f3c9abfb94d6fd190cf2d0",
				updated_date: "2015-01-21T21:12:42.448Z",
				active: true,
				address: [
					"p.o. box 10"
				],
				asset_size: 280,
				branches: 8,
				city: "Fulton",
				country: "US",
				created_by: "00000000-0000-0000-0000-000000000000",
				created_date: "2010-08-24T11:08:37.170Z",
				display_name: "The Callaway Bank",
				domain: [
					"callawaybank.com"
				],
				fi_type: "bank",
				legal_name: "Avidbank",
				phone: "5736423322",
				postal_code: "65251",
				regulatory_agencies: [
					"FRB"
				],
				routing_number: "081501696",
				state: "MO",
				updated_by: "00000000-0000-0000-0000-000000000000",
				website: "http://callawaybank.com",
				_type: "fi_org"
			}
		}, {
			id: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
			updated_date: "2015-01-21T21:29:14.806Z",
			access_token: null,
			active: true,
			created_by: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
			created_date: "05/22/2015",
			email: "paula.bertels@callawaybank.com",
			email_preferences: {
				daily_digest: true,
				documents: true,
				qna: true,
				reviews: true,
				webinars: true
			},
			first_name: "Suzette",
			last_name: "Junier",
			legacy_user_id: "7651",
			phone: [
				"5735926378"
			],
			publish_email: false,
			publish_phone: false,
			title: "Chief Compliance Officer",
			updated_by: "00000000-0000-0000-0000-000000000000",
			years_banking_experience: 0,
			_type: "person",
			full_name: "Suzette Junier",
			password: null,
			org: {
				id: "6e811a1f66f3c9abfb94d6fd190cf2d0",
				updated_date: "2015-01-21T21:12:42.448Z",
				active: true,
				address: [
					"p.o. box 10"
				],
				asset_size: 440,
				branches: 8,
				city: "Fulton",
				country: "US",
				created_by: "00000000-0000-0000-0000-000000000000",
				created_date: "2010-08-24T11:08:37.170Z",
				display_name: "Q2ebanking",
				domain: [
					"callawaybank.com"
				],
				fi_type: "bank",
				legal_name: "Q2ebanking",
				phone: "5736423322",
				postal_code: "65251",
				regulatory_agencies: [
					"FRB"
				],
				routing_number: "081501696",
				state: "TX",
				updated_by: "00000000-0000-0000-0000-000000000000",
				website: "http://callawaybank.com",
				_type: "fi_org"
			}
		}]
	},
	httpContext: httpContext,
	config: require('../../tmp/js/code_gen/config.js'),
	page_model: {
		title: 'Bank and Credit Union Vendor Management Risk Assessment Software for FFIEC & OCC Compliance | CBANC Network',
		stylesheets: [
			'./index.css'
		],
    breadcrumbs: [{
      text: 'Home',
      href: '/'
    }, {
      text: 'Vendor Management',
      href: '#'
    }],
		header: {
			mobile_menu_expanded: false,
			mobile_user_menu_expanded: false,
			menu_items: [{
				text: "Documents",
				href: "/documents"
			}, {
				text: "Q&A",
				href: "/questions"
			}, {
				text: "Products",
				href: "/products"
			}, {
				text: "Education",
				href: "/education"
			}, {
				text: "Vendor Management",
				href: "/vendormanagement",
        active: true
			}],
			sign_in: {
				text: "Sign In",
				href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
			},
			sign_out: {
				text: "Sign Out",
				href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
			},
			join: {
				text: "Join For Free",
				href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
			}
		},
	}
};
