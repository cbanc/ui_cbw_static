var h = require('mercury').h;
var anchor = require('ui_link');
var ui_assets = require('ui_assets');

module.exports = function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var assets = ui_assets(state);

	return h('section#hero', [
		h('header', [
      h("span",
      	assets('stonecastle/stonecastle_logo.png', {alt: 'StoneCastle Financial Corp.'})
    	),
			h('h1', 'Thanks!'),
			h('p', 'Representatives from StoneCastle will be in touch shortly')
		])
	]);
};
