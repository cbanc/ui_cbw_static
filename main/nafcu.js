var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var assets = require('ui_assets');

module.exports = versionify(function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};

	return h('article', [

    h('h3.section_title',
      h('span', "NAFCU and CBANC Network form strategic alliance")
    ),

		h('section.story', [
			h('article.quote', [
				h('p', '"CBANC is a top-notch resource for credit unions that are looking to maximize their budgets without compromising their commitment to excellence."'),
				h('div.citation', [
					h('img', {
						src: 'https://static.cbancnetwork.com/Public/higgins/nafcu/B_Dan_Berger.jpg?',
						alt: 'B. Dan Berger - President & CEO, NAFCU'
					}),
					h('p', 'B. Dan Berger'),
					h('p', 'President & CEO, NAFCU'),
				])
			]),
			h('p.lead', [
				'NAFCU and CBANC Network have formed a strategic alliance that will allow NAFCU-member credit unions to share policies, procedures, agreements and best practices in real time.',
				anchor({
					text: 'Read full story',
					href: '#'
				})
			])
		]),

    h('h3.section_title',
      h('span', 'Compliance Policy Sharing with CBANC')
    ),

		h('section.story', [
			h('article.quote', [
				h('p', '"Through CBANC\'s innovative solutions, compliance officers, risk managers and auditors can now tap into a massive reservoir of policies, procedures, and answers. At no cost."'),
				h('div.citation', [
					h('img', {
						src: 'https://static.cbancnetwork.com/Public/higgins/nafcu/Anthony_Demangone.jpg',
						alt: 'Anthony Demangone - Executive Vice President & COO, NAFCU'
					}),
					h('p', 'Anthony Demangone'),
					h('p', 'Executive Vice President & Chief Operating Officer, NAFCU'),
				])
			]),
			h('p.lead', [
				"Demands on credit unions are higher than ever before. It's harder for your limited resources to be used where they matter most: your members. That's why NAFCU is pleased to endorse CBANC.",
				anchor({
					text: 'Read full story',
					href: '#'
				})
			])
		]),

		h('hr'),

		h('section#join', [
			anchor({
				text: 'Join for free',
				href: '#'
			})
		])
	]);
});
