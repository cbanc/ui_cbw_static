var httpContext = {
  controller: {
    name: 'static_500'
  }
};

module.exports = {
  view_model: {},
  httpContext: httpContext,
  config: require('../../tmp/js/code_gen/config.js'),
  page_model: {
    title: 'Error 500',
    stylesheets: [
      './index.css'
    ],
    header: false,
    footer: false
  }
};
