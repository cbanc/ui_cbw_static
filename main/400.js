var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');

module.exports = versionify(function (state) {
  state = state || {};
  var view_model = state.view_model || {};
  var page_model = state.page_model || {};

  return h('div.error_400', [

    h('header',
      h('div.wrapper', [

        h('h1', "400"),

        h('h2', [
          "Oops, the page you're looking for does not exist.",
          h("br"),
          h("br"),
          "You may want to head back to the ",
          anchor({
            text: 'homepage',
            href: '#'
          }),
          ".",
          h("br"),
          "If you think something is broken, please ",
          anchor({
            text: 'report a problem',
            href: '#'
          }),
          ".",
        ])
      ])
    ),

    h('footer',
      h('div.wrapper',
        h('p', 'Copyright © 2008 - 2015 CBANC Network.')
      )
    )
  ]);
});
