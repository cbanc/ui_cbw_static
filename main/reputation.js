var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');

module.exports = versionify(function (state) {
  state = state || {};
  var view_model = state.view_model || {};
  var page_model = state.page_model || {};

  return h('div', [

    h('article', [
      h('p', 'Your reputation score is a measurement of how much the CBANC community trusts you and views you as a thought leader.'),
      h('p', 'Reputation is earned by helping your peers. The primary way to gain reputation is by posting great documents and useful answers that are thanked by your peers. Every time you are thanked for your contribution you gain reputation.'),
      h('h2', 'Start Building Yours Today'),
      anchor({
        text: 'Upload a Document',
        href: '/documents/create'
      }, 'upload_doc'),
      anchor({
        text: 'Answer a Question',
        href: '/questions'
      }, 'answer_question'),
      h('p', 'All CBANC members start with one Thanks!, just for joining. Clicking "Thanks" on your own answer, document, or review does not gain you any reputation.')
    ]),

    h('aside', [
      h('h4', 'Your Reputation Timeline'),
      h('p', 'Reputation Timeline will go here...')
    ])
  ])
});
