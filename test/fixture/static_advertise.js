var httpContext = {
	controller: {
		name: 'static_advertise'
	}
};

module.exports = {
	view_model: {},
	httpContext: httpContext,
	config: require('../../tmp/js/code_gen/config.js'),
	page_model: {
		title: 'Advertise with us ',
		stylesheets: [
			'./index.css'
		],
		page_title: "Advertise with CBANC",
		breadcrumbs: [{
			text: 'Home',
			href: '/'
		}, {
			text: 'Advertise with Us',
			href: '#'
		}],
		header: {
			mobile_menu_expanded: false,
			mobile_user_menu_expanded: false,
			menu_items: [{
				text: "Documents",
				href: "/documents"
			}, {
				text: "Q&A",
				href: "/questions"
			}, {
				text: "Products",
				href: "/products"
			}, {
				text: "Education",
				href: "/education"
			}, {
				text: "Vendor Management",
				href: "/vendormanagement"
			}],
			sign_in: {
				text: "Sign In",
				href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
			},
			sign_out: {
				text: "Sign Out",
				href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
			},
			join: {
				text: "Join For Free",
				href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
			}
		},
	}
};