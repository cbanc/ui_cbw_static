var p = require('./package.json');
var package_name = p.name + "@" + p.version;
var attr_name = "data-cbanc"
module.exports = function (fn) {
  return function () {
    var vnode = fn.apply(this, arguments);
    if (vnode) {
      vnode.properties = vnode.properties || {};
      vnode.properties.attributes = vnode.properties.attributes || {};
      vnode.properties.attributes[attr_name] = package_name;
    }
    return vnode;
  }
}