var toHTML = require('vdom-to-html');
var assert = require('assert');
var _ = require('lodash');

var data = require('../fixture/vendor_solutions.js');

// ui_page needs a page table
var render = require('ui_page')(require('../../main/vendor_solutions.js'));


suite('Vendor Solutions Page Render', function () {

	test('Vendor Solutions Page Render', function () {
		var dom;

		try {
			dom = render(data);
		} catch (e) {
			assert.ifError(e);
		}

		console.log(toHTML(dom));
	});

});