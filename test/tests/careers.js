var toHTML = require('vdom-to-html');
var assert = require('assert');
var _ = require('lodash');

var data = require('../fixture/careers.js');

// ui_page needs a page table
var render = require('ui_page')(require('../../main/careers.js'));


suite('Careers Page Render', function () {

	test('Careers Page Render', function () {
		var dom;

		try {
			dom = render(data);
		} catch (e) {
			assert.ifError(e);
		}

		console.log(toHTML(dom));
	});

});