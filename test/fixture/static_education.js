var httpContext = {
  controller: {
    name: 'static_education'
  }
};

module.exports = {
  view_model: {
    show_webinars: false,
    testimonials: [{
      id: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
      updated_date: "2015-01-21T21:29:14.806Z",
      access_token: null,
      active: true,
      created_by_user: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
      created_date: "05/22/2015",
      email: "paula.bertels@callawaybank.com",
      email_preferences: {
        daily_digest: true,
        documents: true,
        qna: true,
        reviews: true,
        webinars: true
      },
      first_name: "Paula",
      last_name: "Bertels",
      legacy_user_id: "7651",
      phone: [
        "5735926378"
      ],
      publish_email: false,
      publish_phone: false,
      title: "Risk Management",
      updated_by: "00000000-0000-0000-0000-000000000000",
      years_banking_experience: 0,
      _type: "person",
      full_name: "Paula Bertels",
      password: null,
      org: {
        id: "6e811a1f66f3c9abfb94d6fd190cf2d0",
        updated_date: "2015-01-21T21:12:42.448Z",
        active: true,
        address: [
          "p.o. box 10"
        ],
        asset_size: 280,
        branches: 8,
        city: "Fulton",
        country: "US",
        created_by_user: "00000000-0000-0000-0000-000000000000",
        created_date: "2010-08-24T11:08:37.170Z",
        display_name: "The Callaway Bank",
        domain: [
          "callawaybank.com"
        ],
        fi_type: "bank",
        legal_name: "The Callaway Bank",
        phone: "5736423322",
        postal_code: "65251",
        regulatory_agencies: [
          "FRB"
        ],
        routing_number: "081501696",
        state: "MO",
        updated_by: "00000000-0000-0000-0000-000000000000",
        website: "http://callawaybank.com",
        _type: "fi_org"
      }
    }, {
      id: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
      updated_date: "2015-01-21T21:29:14.806Z",
      access_token: null,
      active: true,
      created_by_user: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
      created_date: "05/22/2015",
      email: "paula.bertels@callawaybank.com",
      email_preferences: {
        daily_digest: true,
        documents: true,
        qna: true,
        reviews: true,
        webinars: true
      },
      first_name: "Steven",
      last_name: "Gerrard",
      legacy_user_id: "7651",
      phone: [
        "5735926378"
      ],
      publish_email: false,
      publish_phone: false,
      title: "Risk Management",
      updated_by: "00000000-0000-0000-0000-000000000000",
      years_banking_experience: 0,
      _type: "person",
      full_name: "Steven Gerrard",
      password: null,
      org: {
        id: "6e811a1f66f3c9abfb94d6fd190cf2d0",
        updated_date: "2015-01-21T21:12:42.448Z",
        active: true,
        address: [
          "p.o. box 10"
        ],
        asset_size: 440,
        branches: 8,
        city: "Fulton",
        country: "US",
        created_by_user: "00000000-0000-0000-0000-000000000000",
        created_date: "2010-08-24T11:08:37.170Z",
        display_name: "The Callaway Bank - Investments",
        domain: [
          "callawaybank.com"
        ],
        fi_type: "bank",
        legal_name: "The Callaway Bank",
        phone: "5736423322",
        postal_code: "65251",
        regulatory_agencies: [
          "FRB"
        ],
        routing_number: "081501696",
        state: "TX",
        updated_by: "00000000-0000-0000-0000-000000000000",
        website: "http://callawaybank.com",
        _type: "fi_org"
      }
    }]
  },
  httpContext: httpContext,
  config: require('../../tmp/js/code_gen/config.js'),
  page_model: {
    title: 'CBANC Education',
    stylesheets: [
      './index.css'
    ],
    header: {
      mobile_menu_expanded: false,
      mobile_user_menu_expanded: false,
      menu_items: [{
        text: "Documents",
        href: "/documents"
      }, {
        text: "Q&A",
        href: "/questions"
      }, {
        text: "Products",
        href: "/products"
      }, {
        text: "Education",
        href: "/education",
        active: true
      }, {
        text: "Vendor Management",
        href: "/vendormanagement"
      }],
      sign_in: {
        text: "Sign In",
        href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
      },
      sign_out: {
        text: "Sign Out",
        href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
      },
      join: {
        text: "Join For Free",
        href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
      }
    },
  }
};
