var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');

module.exports = versionify(function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var contact_us_action = view_model.contact_us_action || {};

	return h('article.advertise_main', [
		h('p', 'Vendor identification, due diligence, and selection are critical points for community banks and credit unions. CBANC is committed to making it easier for community banks/credit unions and their service providers to find one another.'),
		h('p', "CBANC is currently exploring partnership, business development, and advertising opportunities for bank/credit union service providers. If you're interested, we'd love to start a dialogue with you."),
		anchor({
			text: 'Contact us',
			href: contact_us_action.href || '#'
      }, 'contact_us')
	])
});
