var toHTML = require('vdom-to-html');
var assert = require('assert');
var _ = require('lodash');

var data = require('../fixture/support.js');

// ui_page needs a page table
var render = require('ui_page')(require('../../main/support.js'));


suite('Support Page Render', function () {

	test('Support Page Render', function () {
		var dom;

		try {
			dom = render(data);
		} catch (e) {
			assert.ifError(e);
		}

		console.log(toHTML(dom));
	});

});