var h = require('mercury').h;
var anchor = require('ui_link');
var ui_assets = require('ui_assets');

module.exports = function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var assets = ui_assets(state);

	return h('section#hero', [
		h('header', [
      assets('vs/vs_dashboard.png', {alt: 'Establish a presence for your business in CBANC with Vendor Pages'}),
			h('h1', 'Establish a presence for your business in CBANC with Vendor Pages'),
			h('h2', 'Claim your page, describe your products & services and gain exposure to decision makers.'),
			anchor({
				text: 'Join for Free',
				href: '#'
			})
		])
	]);
};
