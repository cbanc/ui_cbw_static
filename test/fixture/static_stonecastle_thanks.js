var httpContext = {
	controller: {
		name: 'static_stonecastle_thanks'
	}
};

module.exports = {
	user: {
		person: {
			full_name: 'Bill Murray',
			reputation: {
				thanks_count: 35
			}
		},
		logged_in: false
	},
	view_model: {
		documents: [{
      title: "E-banking risk assessment",
      synopsis: "Three odd-ball scientists get kicked out of their cushy positions at a university in New York City where they studied the occult. They decide to set up shop in an old firehouse and become Ghostbusters, trapping pesky ghosts, spirits, haunts, and poltergeists for money. They wise-crack their way through the city, and stumble upon a gateway to another dimension, one which will release untold evil upon the city. The Ghostbusters are called on to save the Big Apple.",
      synopsis_limit: 15,
      created_date: new Date(Date.now() - (3600 * 4 * 1000)),
      file_extension: ".doc",
      tags: [{
        text: 'dogs',
        href: '/search/dogs'
      }, {
        text: 'cats',
        href: '/search/cats'
      }, {
        text: 'pets',
        href: '/search/pets'
      }],
      url: 'http://www.imdb.com/title/tt0087332/',
      created_by: {
        full_name: "Bill Murray",
        first_name: "Bill",
        last_name: "Murray",
        id: "1234567890",
        title: "VP of Ectoplasm",
        reputation: 25
      },
      org: {
        display_name: "Guerilla Dog Party",
        asset_size: 132,
        state: "NY"
      },
      thanks_count: 4,
      comments_count: 10,
      downloads_count: 12000
    }, {
      title: "Remote Deposit Capture Annual Customer Audit and Training",
      synopsis: "Five years after the events of the first film, the Ghostbusters have been plagued by lawsuits and court orders, and their once-lucrative business is bankrupt. However, when Dana begins to have ghost problems again, the boys come out of retirement only to be promptly arrested. The Ghostbusters discover that New York is once again headed for supernatural doom, with a river of ectoplasmic slime bubbling beneath the city and an ancient sorcerer attempting to possess Dana's baby and be born anew.",
      synopsis_limit: 15,
      created_date: new Date(Date.now() - (86400 * 2 * 1000)),
      file_extension: ".pdf",
      url: 'http://www.imdb.com/title/tt0097428/',
      created_by: {
        full_name: "Rick Moranis",
        first_name: "Rick",
        last_name: "Moranis",
        id: "1234567890",
        title: "Spooky Desk Clerk",
        reputation: 150
      },
      org: {
        display_name: "Guerilla Cat Party",
        asset_size: 233,
        state: "NY"
      },
      thanks_count: 1200,
      comments_count: 200,
      downloads_count: -1
    }, {
      title: "Remote Deposit Capture Annual Customer Audit and Training",
      created_date: new Date(Date.now() - (86400 * 2 * 1000)),
      file_extension: ".pdf",
      url: 'http://www.imdb.com/title/tt0097428/',
      created_by: {
        full_name: "Rick Moranis",
        first_name: "Rick",
        last_name: "Moranis",
        id: "1234567890",
        title: "Spooky Desk Clerk",
        reputation: 150
      },
      org: {
        display_name: "Guerilla Cat Party",
        asset_size: 233,
        state: "NY"
      },
      thanks_count: 1200,
      comments_count: 200,
      downloads_count: -1
    }, {
      title: "EDD Information Sheet for Nongovernmental organization NGO or Charities",
      created_date: "4/22/2015",
      file_extension: ".xls",
      tags: ["cats", "pets", "horses", "cows", "chickens", "people", "llamas"],
      url: 'http://www.imdb.com/title/tt1289401/',
      created_by: {
        full_name: "Judd Apatow",
        first_name: "Judd",
        last_name: "Apatow",
        id: "1234567890",
        title: "Movie Director",
        reputation: 886
      },
      org: {
        display_name: "Movie Directors R Us",
        asset_size: 6004,
        state: "NY"
      },
      thanks_count: 34,
      comments_count: 3432,
      downloads_count: 65650
    }]
	},
	httpContext: httpContext,
  config: require('../../tmp/js/code_gen/config.js'),
	page_model: {
		title: 'Unlock a new source of capital for your community bank with StoneCastle Financial Corp.',
		stylesheets: [
			'./index.css'
		],
		header: {
			mobile_menu_expanded: false,
			mobile_user_menu_expanded: false,
			menu_items: [{
				text: "Documents",
				href: "/documents"
			}, {
				text: "Q&A",
				href: "/questions"
			}, {
				text: "Products",
				href: "/products"
			}, {
				text: "Education",
				href: "/education"
			}, {
				text: "Vendor Management",
				href: "/vendormanagement"
			}],
			sign_in: {
				text: "Sign In",
				href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
			},
			sign_out: {
				text: "Sign Out",
				href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
			},
			join: {
				text: "Join For Free",
				href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
			}
		},
	}
};
