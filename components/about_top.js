var h = require('mercury').h;
var anchor = require('ui_link');
var ui_assets = require('ui_assets');

module.exports = function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var assets = ui_assets(state);

	return h('section#hero', [
		h('header', [
      assets('cbanc-logo-blue.svg', {alt: 'CBANC Network'}),
			h('h1', 'The professional network for bank and credit union professionals'),
			h('h2', 'Collaboration. Exchange. Community')
		])
	]);
};
