var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');

module.exports = versionify(function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};

	return h('article', [

		h('section#main_column', [

			h('p', 'We are the professional network for the banking industry, powering the largest online community of banks and credit unions in the world. We exist to keep community financial institutions independent and thriving. Every business day, CBANC helps thousands of verified financial professionals and their institutions make more intelligent vendor decisions, navigate regulatory compliance challenges, and answer questions.'),
			h('p', 'Our software leverages the network effects inherent in our community, enabling our members and the vendors that serve them to work together to solve problems. The results are more efficient operations, the ability to better serve customers, and an improved competitive position for our members and the US banking system.'),
			h('h4', 'We currently have one open position. See below.'),

			h('section#openings', [

				h('h2', 'Inside Sales Representative'),
				h('h4', 'Who you are:'),

				h('p', 'As Inside Sales Representative, you...'),
				h('ul', [
					h('li', 'Are a self-starter who can multi-task, wear a lot of hats, and learn quickly' ),
					h('li', 'Are coachable and are passionate about a career in sales'),
					h('li', "Pay attention to detail and don't let any opportunity pass you by"),
					h('li', 'Have a sense of urgency. "If you\'re standing still you\'re going backwards."'),
					h("li", "Have a keen understanding of B2B sales and buying processes"),
					h("li", "Are a leader. We will look to you to build out our sales team and we need someone who can be a player/coach"),
					h("li", "Have a thick skin. You look at rejection as a challenge not a disappointment"),
					h("li", "Are a \"hunter\" who is intrinsically motivated by the thrill of a sale and will stop at nothing to achieve results"),
					h("li", "Are a team player who communicates effectively and fits well within our small but growing team")
				]),

				h("h4", "Where you come in:"),
				h("p", "We need you to support CBANC's Compliance Solutions (SaaS) offering. This includes a focus on the entire sales cycle including qualification of leads, development of qualified opportunities, and working together with the Director of Business Development until close. We expect you to exceed monthly revenue goals. You'll collect product feedback and help our product team prioritize future development work. We expect you to manage all contacts and leads in Infusionsoft to accurately and efficiently process all opportunities. After proven success, we expect you to grow a successful sales team and manage them accordingly."),

				h("h4", "Skills and experience preferred for this role:"),
				h("ul", [
					h("li", "1-3 years Software/SaaS sales experience"),
					h("li", "Background in financial services and/or fintech"),
					h("li", "4-year college degree"),
					h("li", "Understanding of the sales cycle and process of lead generation (cold calling experience a plus)"),
					h("li", "Prior experience using Infusionsoft or other CRM")
				])
			])
		]),

		h("aside#culture", [

			h("h3", "Culture"),
			h("p", "We work smart. We have fun. We care a lot."),
			h("p", "If you think you have what it takes, contact our Dir. of Business Development, Cara Hayward."),
			h("ul", [
				h("li", "An incredible work environment - fun, casual, fast-paced environment"),
				h("li", "Health, dental, vision, disability, and 401K benefits"),
				h("li", "Flexible PTO"),
				h("li", "Fresh, healthy lunches provided three days a week"),
				h("li", "Unlimited snacks and drinks")
			]),
			anchor({
				href: 'mailto:chayward@cbancnetwork.com',
				text: 'Email Cara'
			})
		])
	]);
});
