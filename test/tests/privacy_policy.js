var toHTML = require('vdom-to-html');
var assert = require('assert');
var _ = require('lodash');

var data = require('../fixture/privacy_policy.js');

// ui_page needs a page table
var render = require('ui_page')(require('../../main/privacy_policy.js'));


suite('Privacy Policy Page Render', function () {

	test('Privacy Policy Page Render', function () {
		var dom;

		try {
			dom = render(data);
		} catch (e) {
			assert.ifError(e);
		}

		console.log(toHTML(dom));
	});

});