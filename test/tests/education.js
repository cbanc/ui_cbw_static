var toHTML = require('vdom-to-html');
var assert = require('assert');
var _ = require('lodash');

var data = require('../fixture/education.js');

// ui_page needs a page table
var render = require('ui_page')(require('../../main/education.js'));


suite('Education Page Render', function () {

	test('Education Page Render', function () {
		var dom;

		try {
			dom = render(data);
		} catch (e) {
			assert.ifError(e);
		}

		console.log(toHTML(dom));
	});

});