var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');

module.exports = versionify(function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};

	return h('div.error_500', [

		h('header',
			h('div.wrapper', [

	      h('h1', "500"),

	      h('h2', [
	        "There's a temporary problem with CBANC ",
	      	h("br"),
	        "that we're working to fix right now!",
	      	h("br"),
	      	h("br"),
	        "If this issue continues to occur, please contact our ",
	        anchor({
	          text: 'support team',
	          href: '#'
	        }),
	        ".",
	      ])
			])
		),

		h('footer',
		  h('div.wrapper',
		    h('p', 'Copyright © 2008 - 2015 CBANC Network.')
		  )
		)
	]);
});
