var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var signature = require('ui_signature/components/public.js');
var ui_assets = require('ui_assets');
var section = require('../components/content_section.js');

var sig_check = function (testimonial) {
  if (testimonial) {
    var org = testimonial.org;
    var created_date = testimonial.created_date;
    return signature(testimonial, org, created_date, 'Quoted');
  }
  return h('p.cbanc_member', 'CBANC Member');
}

module.exports = versionify(function (state) {
  state = state || {};
  var view_model = state.view_model || {};
  var page_model = state.page_model || {};
  var testimonials = view_model.testimonials || {};
  var assets = ui_assets(state);

  return h('article#vm_public', [

    h('section#sections', [
      section({
        html_class: 'manage',
        dark: false,
        href: state.view_model.sign_up_url,
        button_text: 'Schedule A Demo',
        side: 'right',
        title: 'Manage your vendors with ease',
        content: h("ul", [
          h("li", "Now you can securely manage all of your vendors in one place, organized by risk assessment. Upload contracts and documents and receive email notifications for contract expiration."),
          h("li", "Stay more connected across your company by creating an internal list of contacts responsible for managing specific vendors and an easily accessible list of vendor contacts."),
          h("li", "Say goodbye to clunky Excel spreadsheets, make your Compliance Officer smile and impress your Board.")
        ])
      }),

      section({
        html_class: 'compilance',
        dark: true,
        href: state.view_model.sign_up_url,
        button_text: 'Get More Info',
        side: 'left',
        title: 'Community-powered compliance',
        content: h("ul", [
          h("li", "More than just another VM solution, CBANC Vendor Management taps into the collective power of the CBANC community to provide you with even more insight and up-to-date vendor information."),
          h("li", "Stay current on your existing vendors by viewing member Product Reviews."),
          h("li", "Get help in the vendor selection process by comparing vendors and getting real-time recommendations from the Community.")
        ])
      })
    ]),

    h('section#testimonials',
      h('div.wrapper', [
        h('section.banner', 'Vendor Management Testimonials'),
        h('div.quotes', [
          h('article', [
            h('section.quote', [
              '"Much better than a manual process. The reports and automatic notifications are very helpful! The software is easy to use. CBANC and the community have met all my expectations."'
            ]),
            h('section.sig', [
              sig_check(testimonials[0])
            ])
          ]),
          h('article', [
            h('section.quote', '"A great vision for leveraging the power of the CBANC community."'),
            h('section.sig', [
              sig_check(testimonials[1])
            ])
          ])
        ])
      ])
    ),

    h('section#quote_cta',
      h('div.wrapper',
        h('article.quote', [

          h('div.avatar', [
            h('figure', [
              assets('about/cara_hayward.jpg', {
                alt: 'Cara Hayward - CBANC Director of Business Development'
              }),
            ]),
          ]),
          h('div.details', [
            h('p', '"If you have any questions about Vendor Management by CBANC just let me know. We are proud of what we build and I look forward to sharing it with you."'),
            sig_check(view_model.cara),
            anchor({
              text: 'Contact Cara',
              href: 'mailto:chayward@cbancnetwork.com?Subject=Interested%20in%20CBANC%20Vendor%20Management'
            }, 'contact_btn')
          ]),
        ])
      )
    )
  ]);
});
