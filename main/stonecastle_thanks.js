var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var assets = require('ui_assets');
var documents_list = require('ui_documents/components/list.js');


module.exports = versionify(function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};

	return h('article', [

		h('h3.section_title',
			h('span', "While you're here, check out these trending documents on CBANC:")
		),

		h('div.wrapper', [

			documents_list(view_model.documents || [], 'card', {
				tags_count: 2
			}),

			h('hr'),

			h('div.bottom_section',
				anchor({
					text: 'Back to StoneCastle informational page',
					href: '/promotions/stonecastle'
				}, 'button')
			)
		])
	]);
});