var httpContext = {
  controller: {
    name: 'static_privacy_policy'
  }
};

module.exports = {
  view_model: {},
  httpContext: httpContext,
  config: require('../../tmp/js/code_gen/config.js'),
  page_model: {
    title: 'Privacy Policy',
    stylesheets: [
      './index.css'
    ],
    page_title: "Privacy Policy",
    breadcrumbs: [{
      text: 'Home',
      href: '/'
    }, {
      text: 'Support',
      href: '#'
    }, {
      text: 'Privacy Policy',
      href: '#'
    }],
    header: {
      mobile_menu_expanded: false,
      mobile_user_menu_expanded: false,
      menu_items: [{
        text: "Documents",
        href: "/documents"
      }, {
        text: "Q&A",
        href: "/questions"
      }, {
        text: "Products",
        href: "/products"
      }, {
        text: "Education",
        href: "/education"
      }, {
        text: "Vendor Management",
        href: "/vendormanagement"
      }],
      sign_in: {
        text: "Sign In",
        href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
      },
      sign_out: {
        text: "Sign Out",
        href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
      },
      join: {
        text: "Join For Free",
        href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
      }
    },
  }
};
