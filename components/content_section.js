var h = require('mercury').h;
var anchor = require('ui_link');
var class_appender = require('ui_helpers')('class_appender');

module.exports = function (options) {
  options = options || {};

  var name = options.html_class;

  var html_class = class_appender(options.html_class, options.side, (options.dark ? 'dark' : null));

  return h('article' + html_class, [

    h('a', {
      className: 'bookmark',
      attributes: {
        name: name,
        rel: 'bookmark'
      }
    }),

    h('div.wrapper', [
      h('div.content',
        h('div.content_inner', [

          h('h2', options.title),

          options.content,

          anchor({
            text: options.button_text || 'Join for free',
            href: options.href
          }, 'button')
        ])
      ),

      h('div.feature_image')
    ])
  ]);
};
