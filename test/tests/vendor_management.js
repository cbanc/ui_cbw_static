var toHTML = require('vdom-to-html');
var assert = require('assert');
var _ = require('lodash');

var data = require('../fixture/vendor_management.js');

// ui_page needs a page table
var render = require('ui_page')(require('../../main/vendor_management.js'));


suite('Vendor Management Page Render', function () {

	test('Vendor Management Page Render', function () {
		var dom;

		try {
			dom = render(data);
		} catch (e) {
			assert.ifError(e);
		}

		console.log(toHTML(dom));
	});

});