var h = require('mercury').h;
var versionify = require('../versionify.js');
var signature = require('ui_signature');

module.exports = versionify(function (list) {

	if (list && list.length) {

		return h('table.leaderboard_list', [
			h('thead', [
				h('tr', [
					h('th.place', ['Place']),
					h('th.signature', ['Member']),
					h('th.rep_score', ['Rep Score'])
				])
			]),
			list.map(

				function (p) {
					var user = {
						full_name: p.first_name + ' ' + p.last_name,
						title: p.title,
						key: p.key
					};

					var org = (p.orgs || [])[0] || null;

					return h('tr', [
						h('td.place', 'x'),
						h('td.signature', [signature(user, org)]),
						h('td.rep_score', ['Reputation Score: ' + p.reputation_score])
					])
				}
			)
		]);
	}

	return h('h2', 'Leaderboard is currently unavailable. Our engineers are looking into it...')
});