var gulp = require('gulp');

var options = {
  name: 'static',
  default_view: 'about',
  page_module: true,
  page_table: function (view) {
  	var page_table = {};
  	try {
  	  page_table = {
  	  	top_section: require('./components/' + view + '_top.js')
  	  };
  	} catch (e) {}
    return page_table;
  }
};

var ui_gulp = require('ui_gulp')(options);

gulp.task('default', ui_gulp.default);

gulp.task('watch', ui_gulp.watch);