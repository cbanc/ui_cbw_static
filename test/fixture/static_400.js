var httpContext = {
  controller: {
    name: 'static_400'
  }
};

module.exports = {
  view_model: {},
  httpContext: httpContext,
  config: require('../../tmp/js/code_gen/config.js'),
  page_model: {
    title: 'Error 400',
    stylesheets: [
      './index.css'
    ],
    header: false,
    footer: false
  }
};
